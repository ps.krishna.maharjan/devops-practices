## Package (specification and the body)

```
create or replace package krishna as

PROCEDURE P_PRINT_HELLO(my_name in varchar2);

PROCEDURE P_CREATE_TAB_EMP;

PROCEDURE P_CREATE_TAB_DEP;

PROCEDURE P_INSERT_DATA;

PROCEDURE P_PRINT_EMP_S(e_name in varchar2);

PROCEDURE P_UPDATE_SAL (e_num in number, e_sal in NUMBER);

PROCEDURE P_SQL_FUN;

PROCEDURE P_DELETE_USER (dept_id in NUMBER);

FUNCTION F_DEPT_STAT(P_DEPT_ID IN NUMBER, STAT_ID IN NUMBER, P_OUTPUT OUT VARCHAR2) RETURN VARCHAR2;

FUNCTION F_DEPT10_HR(dept NUMBER, salary NUMBER) RETURN VARCHAR2;

FUNCTION EMP_HR RETURN VARCHAR2;

FUNCTION F_HR_Supervisor RETURN VARCHAR2;

FUNCTION F_HR_REPORT RETURN SYS_REFCURSOR;

FUNCTION F_HR_MANAGER RETURN SYS_REFCURSOR;

FUNCTION F_HR_QUERY RETURN SYS_REFCURSOR;

FUNCTION F_SAL_ROUND RETURN SYS_REFCURSOR;

end krishna;
/

create or replace package body krishna as
-- Procedure 1
    PROCEDURE P_PRINT_HELLO(my_name in varchar2)
    is
    begin
    dbms_output.put_line ('Hello, world!'||my_name);
    end P_PRINT_HELLO;
    
-- Procedure 2
    PROCEDURE P_CREATE_TAB_EMP
    is 
    begin
    EXECUTE IMMEDIATE 'CREATE TABLE EMP (
        EMPNO NUMBER(4) PRIMARY KEY,
        ENAME VARCHAR2(10),
        JOB VARCHAR2(9),
        MGR NUMBER(4),
        HIREDATE DATE,
        SAL NUMBER(7,2),
        COMM NUMBER(7,2),
        DEPTNO NUMBER(2),
        constraint constraint_salary CHECK (SAL > 0),
        CONSTRAINT constraint_comm CHECK (COMM >= 0),
        CONSTRAINT constraint_foreignkey 
            FOREIGN KEY (DEPTNO) 
            REFERENCES DEPT (DEPTNO)
        )';
    END P_CREATE_TAB_EMP;

-- Procedure 3
    PROCEDURE P_CREATE_TAB_DEP
    is 
    begin
    EXECUTE IMMEDIATE 'CREATE TABLE DEPT (
    DEPTNO NUMBER(2) PRIMARY KEY, 
    DNAME VARCHAR2(14),
    LOC VARCHAR2(13),
    CONSTRAINT constraint_deptno_dep CHECK (DEPTNO > 0),
    -- CONSTRAINT constraint_deptno PRIMARY KEY (DEPTNO),
    CONSTRAINT constraint_dname_dep UNIQUE (DNAME)
    )';
    END P_CREATE_TAB_DEP;

-- Procedure 4
    PROCEDURE P_INSERT_DATA 
    AS 
    BEGIN
    INSERT INTO DEPT VALUES (10, 'ACCOUNTING', 'NEW YORK');
    INSERT INTO DEPT VALUES (20, 'RESEARCH', 'DALLAS');
    INSERT INTO DEPT VALUES (30, 'SALES', 'CHICAGO');
    INSERT INTO DEPT VALUES (40, 'OPERATIONS', 'BOSTON');
    INSERT INTO EMP VALUES (7369, 'SMITH', 'CLERK', 7902, to_date('17-DEC-80', 'DD-Mon-YY'), 800, NULL, 2);
    INSERT INTO EMP VALUES (7499, 'ALLEN', 'SALESMAN', 7698, to_date('20-FEB-81', 'DD-Mon-YY'), 1600, 300, 30);
    INSERT INTO EMP VALUES (7521, 'WARD', 'SALESMAN', 7698, to_date('22-FEB-81', 'DD-Mon-YY'), 1250, 500, 30);
    INSERT INTO EMP VALUES (7566, 'JONES', 'MANAGER', 7839, to_date('02-APR-81', 'DD-Mon-YY'), 2975, NULL, 20); 
    INSERT INTO EMP VALUES (7654, 'MARTIN', 'SALESMAN', 7698, to_date('28-SEP-81', 'DD-Mon-YY'), 1250, 1400, 30);
    INSERT INTO EMP VALUES (7698, 'BLAKE', 'MANAGER', 7839, to_date('01-MAY-81', 'DD-Mon-YY'), 2850, NULL, 30);
    INSERT INTO EMP VALUES (7782, 'CLARK', 'MANAGER', 7839, to_date('09-JUN-81', 'DD-Mon-YY'), 2450, NULL, 10);
    INSERT INTO EMP VALUES (7788, 'SCOTT', 'ANALYST', 7566, to_date('19-APR-87', 'DD-Mon-YY'), 3000, NULL, 20);
    INSERT INTO EMP VALUES (7839, 'KING', 'PRESIDENT', NULL, to_date('17-NOV-81', 'DD-Mon-YY'), 5000, NULL, 10);
    INSERT INTO EMP VALUES (7844, 'TURNER', 'SALESMAN', 7698, to_date('08-SEP-81', 'DD-Mon-YY'), 1500, 0, 30);
    INSERT INTO EMP VALUES (7876, 'ADAMS', 'CLERK', 7788, to_date('23-MAY-87', 'DD-Mon-YY'), 1100, NULL, 20);
    INSERT INTO EMP VALUES (7900, 'JAMES', 'CLERK', 7698, to_date('03-DEC-81', 'DD-Mon-YY'), 950, NULL, 30 );
    INSERT INTO EMP VALUES (7902, 'FORD', 'ANALYST', 7566, to_date('03-DEC-81', 'DD-Mon-YY'), 3000, NULL, 20);
    INSERT INTO EMP VALUES (7934, 'MILLER', 'CLERK', 7782, to_date('23-JAN-82', 'DD-Mon-YY'),1300, NULL, 10);
    INSERT INTO EMP VALUES (7777, 'JOLLY', 'CLERK', 7782, to_date('23-JAN-82', 'DD-Mon-YY'),1300, NULL, 40);

    END P_INSERT_DATA;
   
 -- procedure 5   
    PROCEDURE P_PRINT_EMP_S(e_name in varchar2) IS
        SAL_OUT EMP.SAL%TYPE;
        DEPT_OUT DEPT.DNAME%TYPE;
    BEGIN
        SELECT e.SAL, d.DNAME INTO SAL_OUT, DEPT_OUT FROM EMP e JOIN DEPT d ON e.DEPTNO=d.DEPTNO WHERE e.ENAME=e_name;
        dbms_output.put_line ('Salary for Employ '||e_name||' from Department '||DEPT_OUT||' = '||SAL_OUT||'!');
    END P_PRINT_EMP_S;
    
-- procedure 6
    PROCEDURE P_UPDATE_SAL (e_num in number, e_sal in NUMBER) IS
    BEGIN
        UPDATE EMP 
        SET SAL = e_sal
        WHERE EMPNO = e_num;
    END P_UPDATE_SAL;
    
-- procedure 7
    PROCEDURE P_SQL_FUN IS
    SALARY VARCHAR2(242);
    BEGIN
        FOR emp_record IN (SELECT SAL, ENAME FROM EMP order by SAL) LOOP
            SALARY := emp_record.ENAME ||' '|| RPAD('*', ROUND(emp_record.SAL/100), '*');
            dbms_output.put_line(SALARY);
        END LOOP;
    END P_SQL_FUN;
    
-- procedure 8
    PROCEDURE P_DELETE_USER (dept_id in NUMBER) IS
    BEGIN
        DELETE FROM EMP WHERE DEPTNO=dept_id;
    END P_DELETE_USER;
    
-- function 1
    FUNCTION F_DEPT_STAT (P_DEPT_ID IN NUMBER, STAT_ID IN NUMBER, P_OUTPUT OUT VARCHAR2) RETURN VARCHAR2 IS
    depname DEPT.DNAME%TYPE;
    e_count NUMBER;

    BEGIN
        IF STAT_ID= 1 THEN
        SELECT d.DNAME, (select count(*) from EMP WHERE DEPTNO=P_DEPT_ID) INTO depname, e_count FROM EMP e JOIN DEPT d ON e.DEPTNO=d.DEPTNO WHERE e.DEPTNO=P_DEPT_ID AND ROWNUM =1;
        dbms_output.put_line('Department '||depname||' has total number of employ = '||e_count);
        return P_OUTPUT;
        ELSIF STAT_ID= 2 THEN
        SELECT d.DNAME, (select AVG(SAL) from EMP WHERE DEPTNO=P_DEPT_ID) INTO depname, e_count FROM EMP e JOIN DEPT d ON e.DEPTNO=d.DEPTNO WHERE e.DEPTNO=P_DEPT_ID AND ROWNUM =1;
        dbms_output.put_line('Department '||depname||' has avarage salary = '||e_count);
        RETURN P_OUTPUT;
        END IF;
    END F_DEPT_STAT;

-- function 2
    FUNCTION F_DEPT10_HR(dept NUMBER, salary NUMBER) RETURN VARCHAR2 IS 
    response VARCHAR2(244);
    BEGIN
        FOR EMPLOYEE IN (SELECT * FROM EMP WHERE DEPTNO=dept OR SAL>salary) LOOP
            response:= EMPLOYEE.EMPNO||' '|| EMPLOYEE.ENAME||EMPLOYEE.JOB|| EMPLOYEE.MGR||EMPLOYEE.HIREDATE|| EMPLOYEE.SAL|| EMPLOYEE.COMM|| EMPLOYEE.DEPTNO;
            dbms_output.put_line(response);
        END LOOP;
        RETURN NULL;
   END F_DEPT10_HR;

-- fuunction 3
    FUNCTION EMP_HR RETURN VARCHAR2 IS
    salary varchar2(244);
    BEGIN
       FOR name_e IN ( SELECT ENAME FROM EMP WHERE SAL>2000 AND SAL<6000) LOOP
            salary:= name_e.ENAME;
            dbms_output.put_line(salary);
        END LOOP;
        RETURN NULL;
    END EMP_HR;
    
-- function 4
    FUNCTION F_HR_Supervisor RETURN VARCHAR2 IS
    e_count NUMBER;
    e_sal VARCHAR2(244);
    BEGIN
        SELECT COUNT(ENAME), MAX(SAL) INTO e_count, e_sal FROM EMP WHERE JOB='SALESMAN';
        dbms_output.put_line(e_count||' '||e_sal);
    RETURN NULL;
    END  F_HR_Supervisor;

-- function 5
    FUNCTION F_HR_REPORT RETURN SYS_REFCURSOR IS
        v_cursor SYS_REFCURSOR;
    BEGIN
        OPEN v_cursor FOR
        SELECT 
            EMPNO,
            HIREDATE,
            MONTHS_BETWEEN(SYSDATE, HIREDATE) AS MONTHS_EMPLOYED,
            ADD_MONTHS(HIREDATE, 6) AS SIX_MONTH_REVIEW_DATE,
            NEXT_DAY(HIREDATE, 'FRIDAY') AS FIRST_FRIDAY_AFTER_HIREDATE,
            LAST_DAY(HIREDATE) AS LAST_DAY_OF_MONTH_HIRED
        FROM EMP
        WHERE MONTHS_BETWEEN(SYSDATE, HIREDATE) < 200;
        RETURN v_cursor;
    END F_HR_REPORT;
    

-- function 6
    FUNCTION F_HR_MANAGER RETURN SYS_REFCURSOR IS
        v_cursor SYS_REFCURSOR;
    BEGIN
        OPEN v_cursor FOR
        SELECT 
            e.ENAME AS EMPLOYEE,
            e.EMPNO AS EMP#,
            e.DEPTNO AS MGR#,
            d.DNAME AS MANAGER
        FROM EMP e 
        JOIN DEPT d 
        ON e.DEPTNO=d.DEPTNO;
        RETURN v_cursor;
    END F_HR_MANAGER;

-- function 7
    FUNCTION F_HR_QUERY RETURN SYS_REFCURSOR IS
        v_cursor SYS_REFCURSOR;
        john_date EMP.HIREDATE%TYPE;
    BEGIN
        SELECT HIREDATE INTO john_date FROM EMP WHERE ENAME = 'JONES';
        OPEN v_cursor FOR
        SELECT ENAME, HIREDATE FROM EMP WHERE HIREDATE > john_date ORDER BY HIREDATE;
        RETURN v_cursor;
    END F_HR_QUERY;
    
-- function 8 
    FUNCTION F_SAL_ROUND RETURN SYS_REFCURSOR IS
        v_cursor SYS_REFCURSOR;
    BEGIN
        OPEN v_cursor FOR
        SELECT ENAME, ROUND(SAL, 3) FROM EMP;
        RETURN v_cursor;
    END F_SAL_ROUND;
    
end krishna;
/
```
## To execute the above package procedures 

```
exec krishna.P_PRINT_HELLO('Micheal Angelo');
exec krishna.P_CREATE_TAB_DEP;
exec krishna.P_CREATE_TAB_EMP;
EXEC krishna.P_PRINT_EMP_S('ADAMS');
EXEC krishna.P_INSERT_DATA;
exec krishna.P_UPDATE_SAL(7934, 2000);
exec krishna.P_SQL_FUN;
exec krishna.P_DELETE_USER(40);
select krishna.F_DEPT_STAT(1) from dual;
DECLARE 
    dept_name VARCHAR2(100);
BEGIN
    DBMS_OUTPUT.PUT_LINE(krishna.F_DEPT_STAT(10,2, dept_name));
END;
/
BEGIN
    DBMS_OUTPUT.PUT_LINE(krishna.F_DEPT10_HR(10, 3000));
END;
/
BEGIN
    DBMS_OUTPUT.PUT_LINE(krishna.EMP_HR());
END;
/
BEGIN
    DBMS_OUTPUT.PUT_LINE(krishna.F_HR_Supervisor());
END;
/
BEGIN
    DBMS_OUTPUT.PUT_LINE(krishna.F_HR_QUERY());
END;
/
VAR rc REFCURSOR;
EXEC :rc := krishna.F_HR_REPORT;
PRINT rc;

VAR rc REFCURSOR;
EXEC :rc := krishna.F_HR_MANAGER;
PRINT rc;

VAR rc REFCURSOR;
EXEC :rc := krishna.F_HR_QUERY;
PRINT rc;

VAR rc REFCURSOR;
EXEC :rc := krishna.F_SAL_ROUND;
PRINT rc;

SET SERVEROUTPUT ON;
```

### For testing 
```select * from DEPT;
select* from EMP;
select * from EMP where DEPTNO=20;
ALTER TABLE EMP 
    DROP CONSTRAINT constraint_foreignkey;
DROP TABLE DEPT;
-- FOR TURNCATING THE TABLES
    TRUNCATE TABLE EMP;
    TRUNCATE TABLE DEPT;
SELECT SAL FROM EMP WHERE EMP.ENAME='SMITH';
SELECT e.SAL, d.DNAME FROM EMP e JOIN DEPT d ON e.DEPTNO=d.DEPTNO WHERE e.ENAME='SMITH';
select RPAD('*', ROUND(SAL/100), '*')||' '|| ENAME "eands"from EMP order by SAL;
SELECT d.DNAME, (select AVG(SAL) from EMP WHERE DEPTNO=20) FROM EMP e JOIN DEPT d ON e.DEPTNO=d.DEPTNO WHERE e.DEPTNO=20 AND ROWNUM =1;
SELECT * FROM EMP WHERE DEPTNO = 10 OR SAL > 4000;
SELECT * FROM  EMP WHERE SAL>2000 AND SAL<6000;
SELECT COUNT(ENAME), MAX(SAL) FROM EMP WHERE JOB='SALESMAN';

 SELECT EMPNO,
        HIREDATE,
        MONTHS_BETWEEN(SYSDATE, HIREDATE) AS MONTHS_EMPLOYED,
        ADD_MONTHS(HIREDATE, 6) AS SIX_MONTH_REVIEW_DATE,
        NEXT_DAY(HIREDATE, 'FRIDAY') AS FIRST_FRIDAY_AFTER_HIREDATE,
        LAST_DAY(HIREDATE) AS LAST_DAY_OF_MONTH_HIRED
    FROM EMP
    WHERE MONTHS_BETWEEN(SYSDATE, HIREDATE) < 200;
```
### To create a database link

```
CREATE DATABASE LINK colleague_db_link
CONNECT TO colleague_username IDENTIFIED BY colleague_password
USING 'colleague_tns_entry';
```
* Give the name of the database link in place of "colleague_db_link".
* Give the username to connect in the db in place of "colleague_username".
* Give the password of the db in the place of "colleague_password".
* Replace the "colleague_tns_entry" with the TNS
or the connection string to the db.
#### To select all department from your colleague database.
```
SELECT * FROM departments@colleague_db_link;
```
* Replace the "departments@colleague_db_link" with the actual database tablename here for example with the table department.

#### To create a materialized view
```
CREATE MATERIALIZED VIEW REPORT_HR_DEPT_MATER_VIEW
AS
SELECT DEPTNO AS DEPARTMENT, ENAME AS EMPLOYEE FROM EMP ORDER BY DEPTNO;
```
#### To execute the materialized view 
```
SELECT * FROM REPORT_HR_DEPT_MATER_VIEW;
```

#### To delete materialized view
```
DROP MATERIALIZED VIEW REPORT_HR_DEPT_MATER_VIEW;
```
#### To Create schedule Job to refresh Above Materialized view every 5 minute  except Friday & Saturday. 
```
BEGIN
  DBMS_SCHEDULER.CREATE_JOB(
    job_name        => 'RR_HR_DEPT_MATER_VIEW',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'BEGIN DBMS_MVIEW.REFRESH("REPORT_HR_DEPT_MATER_VIEW"); END;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'FREQ=MINUTELY; INTERVAL=5; BYDAY=SU,MO,TU,WE,TH',
    enabled         => TRUE,
    auto_drop       => FALSE,
    comments        => 'Job to refresh materialized view REPORT_HR_DEPT_MATER_VIEW every 5 minutes except Fridays and Saturdays'
  );
END;
/
```
#### To check if the job is running or not.
```
SELECT * 
FROM DBA_SCHEDULER_JOBS
WHERE JOB_NAME = 'RR_HR_DEPT_MATER_VIEW'
```
#### Create the following table below:
```
CREATE TABLE ps_orders( id number(5),  quantity number(4),  cost_per_item number(6,2),  total_cost 
number(8,2),  create_date date,  created_by varchar2(10), 
  update_date date, 
  updated_by varchar2(10))
```
#### Create a trigger to insert auto value for the ID column ( for each insert must increase by 1).
```
CREATE SEQUENCE sequence_id
START WITH 1
INCREMENT BY 1;

CREATE OR REPLACE TRIGGER AUTO_INSERT
BEFORE INSERT ON ps_orders
FOR EACH ROW
BEGIN 
    SELECT sequence_id.NEXTVAL INTO :NEW.id FROM dual;
END;
/
```
#### Create a trigger to set create date column with the current date and time.
```
CREATE OR REPLACE TRIGGER set_current_date
BEFORE INSERT ON ps_orders
FOR EACH ROW
BEGIN
    :NEW.create_date:= SYSTIMESTAMP;
END;
/
```
#### Create a trigger to set created by column with the current user who makes the insert.
```
CREATE OR REPLACE TRIGGER set_create_by
BEFORE INSERT ON ps_orders
FOR EACH ROW 
BEGIN 
    :NEW.created_by:= SYS_CONTEXT('USERENV', 'SESSION_USER');
END;
/
```

#### Create a trigger to calculate the total cost column by multiple quantities with cost per item.
```
CREATE OR REPLACE TRIGGER total_cost
BEFORE INSERT OR UPDATE ON ps_orders
FOR EACH ROW
BEGIN 
    :NEW.total_cost:= :NEW.quantity* :NEW.cost_per_item;
END;
/
```
#### Create a trigger to update (update date and update by) columns if the user executes the update statement on the table. 
```
CREATE OR REPLACE TRIGGER update_trigger
BEFORE UPDATE ON ps_orders
FOR EACH ROW
BEGIN
    :NEW.update_date:= SYSTIMESTAMP;
    :NEW.updated_by:= SYS_CONTEXT('USERENV', 'SESSION_USER');
END;
/
```
#### Testing above pl/sql.
```
BEGIN
    UPDATE ps_orders
    SET quantity = 1
    WHERE id = 5 ;
END;
/

BEGIN
    INSERT INTO ps_orders (id, quantity, cost_per_item, total_cost, create_date, created_by, update_date, updated_by)
    VALUES
  (4, 0, 12.50, 350.00, TO_DATE('2024-02-15', 'YYYY-MM-DD'), 'User3', TO_DATE('2024-02-15', 'YYYY-MM-DD'), 'User3');
END;
/

select * from ps_orders;
```
#### - B-Tree index.
```
CREATE TABLE doctor (
    doctor_id NUMBER PRIMARY KEY,
    first_name VARCHAR2(50),
    last_name VARCHHAR2(50),
    dept_statu VARCHAR2(10),
    dept_no NUMBER(10)
);

CREATE INDEX indx_dept_no
ON doctor(dept_no);
```
#### - Bitmap index
```
CREATE BITMAP INDEX indx_dept_status
ON doctor(dept_status);
```
#### Unique index.
```
CREATE UNIQUE INDEX indx_name_unique
ON doctor(name);
```
#### Function based index.
```
CREATE INDEX indx_function
ON doctor(UPPER(first_name)||' '||UPPER(last_name));
```
#### Concatenated index.
```
CREATE INDEX indx_concatenation 
ON doctor(first_name, dept_no);
```