## Dockerfile
1. **Docker build for h2 database**
    ```
    docker build --build-arg hash=20240104-ab8c0e0f -t assignment:20240104-ab8c0e0f .
    ```
2. **Docker run for h2 database**
    ```
    docker run -it --rm -e H2_PROFILE=-Dspring.profiles.active=h2 -e hash=20240104-ab8c0e0f -p 8080:8090 assignment:20240104-ab8c0e0f
    ```
## Docker Compose
1. **To run Docker compose using mysql and spring app**
    ```
    docker compose up
    ```
## Docker push
1. **Login to the dockerhub**
    ```
    docker login –username clususkrishna 
    ```
2. **A prompt will open to enter the password**
3. **Tag the local docker accordingly**
    ```
    docker tag assignment:20240104-ab8c0e0f clususkrishna/assignment:20240104-ab8c0e0f 
    ```
4. **Push the docker image to the dockerhub**
    ```
    docker push clususkrishna/assignment:20240104-ab8c0e0f 
    ```
## Running mysql container and spring app seperately and connecting them in a network
1. Create an entrpoint in Dockerfile to execute mysql.sh
    ```
    ENTRYPOINT [ "./mysql.sh" ]
    ```
2. Add the following command to the mysql.sh file
    ```
    #!/usr/bin/env bash

    java -jar -Dserver.port=8070 -Dspring.datasource.username=clusus -Dspring.datasource.password=123 -Dspring.datasource.url=jdbc:mysql://mysql_db:3306/assignment assignment-20240122-412f4e41.jar
    ```
3. Run the mysql container wiht the name mysql_db
    ```
    docker run --name mysql_db -e MYSQL_ROOT_PASSWORD=P@ssw0rd -e MYSQL_DATABASE=assignment -e MYSQL_USER=clusus -e MYSQL_PASSWORD=123   mysql:8.0
    ```
4. Run the spring app and link the mysql_db container.
    ```
    docker run --name linkcontainers -p 8080:8070 --link mysql_db assignment:20240104-ab8c0e0f
    ```

