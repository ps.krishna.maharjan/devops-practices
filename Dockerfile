# Get the official image to run the jre file 
FROM eclipse-temurin:17-jdk-alpine

# Pass the hash as the argument
ARG hash

# Pass the hash as an environment variable
ENV HASH=$hash

# Copy the jar file from the host location to the container /app/ directory
COPY devops_sample/target/assignment-$HASH.jar /app/

#Creating a volume
VOLUME /app/log

# Change the working directory to the /app directory
WORKDIR /app

# To add new user in the running container
RUN adduser -D clusus

# To change the user from root to another user
USER clusus

# Create an environment variable to pass the h2 profile variable
ENV H2_PROFILE=$H2_PROFILE

# Run the command to run the application
CMD java -jar $H2_PROFILE assignment-$HASH.jar
#ENTRYPOINT ["sh", "-c", "echo changed the user form root to; whoami; java -jar $H2_PROFILE *.jar"]
