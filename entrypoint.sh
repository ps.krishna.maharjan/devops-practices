#!/usr/bin/env bash

JAVA_BIN="$(which java)"

if [[ -z "$1" ]]; then
	$JAVA_BIN -jar $H2_PROFILE $PWD/*.jar
else 
	exec "$@"
fi
